import { useState } from 'react'

const App = () => {
  const [formValue, setFormValue] = useState({
    normal: '',
    texto: '',
    select: '',
    check: false,
    estado: 'Feliz'
  })

  const { normal, texto, select, check, estado } = formValue

  const handleChange = ({ target: { name, type, value, checked } }) => {
    console.log(value)
    setFormValue({
      ...formValue,
      [name]: type === 'checkbox'
        ? checked
        : value
    })
    /*  Alternative
        setFormValue( (state) => ({
          ...state,
          [name]: value
        })) */
  }

  console.log(formValue)

  return (
    <div>
      {normal.length < 5 ? <span>longitud minimo de 5</span> : null}
      <input type='text' name='normal' value={normal} onChange={handleChange} />
      <textarea name='texto' onChange={handleChange} value={texto} />
      <select value={select} name='select' onChange={handleChange}>
        <option disabled={true} value=''>--Seleccione--</option>
        <option value='chanchoFeliz'>Chancho feliz</option>
        <option value='chanchitoFeliz'>Chanchito feliz</option>
        <option value='chanchitoTriste'>Chanchito triste</option>
        <option value='chanchitoNormal'>Chanchito normal</option>
      </select>

      <input
        type='checkbox'
        name='check'
        onChange={handleChange}
        checked={check}
      />
      <div>
        <label>Chancho</label>
        <input
          type='radio'
          value='Feliz'
          name='estado'
          onChange={handleChange}
          checked={estado === 'Feliz'}
        /> Feliz
        <input
          type='radio'
          value='Triste'
          name='estado'
          onChange={handleChange}
          checked={estado === 'Triste'}
        /> Triste
        <input
          type='radio'
          value='Normal'
          name='estado'
          onChange={handleChange}
          checked={estado === 'Normal'}
        /> Normal
      </div>
    </div>
  )
}

export default App
